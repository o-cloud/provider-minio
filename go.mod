module gitlab.com/o-cloud/provider-minio

go 1.16

require (
	github.com/minio/minio-go/v7 v7.0.21
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/provider-library v0.0.0-20220203145642-fc81188a0e1f
)
