package implement

import "testing"

func TestRandStringRune(t *testing.T) {
	ret := randStringRunes(5)
	if len(ret) != 5 {
		t.Fail()
	}
}
