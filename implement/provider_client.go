package implement

import (
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/o-cloud/provider-minio/config"
)

// TODO : Load provider client and attach it to a global variable available through geter function
//        For exemple can be a mongodb client or ftp access.
func LoadProviderClients() {
	LoadMinioClient()
}

var minioClient *minio.Client

func LoadMinioClient() {
	endpoint := config.Config.Service.MinioHost
	accessKeyID := config.Config.Service.MinioAccessKey
	secretAccessKey := config.Config.Service.MinioSecretKey
	useSSL := false

	// Initialize minio client object.
	client, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		panic(err)
	}

	minioClient = client
}

func GetMinioClient() *minio.Client {
	return minioClient
}
