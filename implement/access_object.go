package implement

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"time"

	"github.com/minio/minio-go/v7"
	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/scopes"
	"gitlab.com/o-cloud/provider-minio/config"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// DO NOT TOUCH
type accessObjectController struct {
}

// DO NOT TOUCH
func RegisterAccessObjectRepository() {
	accessObjectController := &accessObjectController{}
	access_object.SetAccessObjectController(accessObjectController)
}

// TODO : Create access object struct with all informations needed to connect on provider
type access struct {
	AccessHost   string `json:"access-host"`
	AccessKey    string `json:"access-key"`
	AccessSecret string `json:"access-secret"`
	Bucket       string `json:"bucket"`
}

// TODO : Implement CreateAccess function that generate and return access object defined before
// You must not touch to function declaration
func (a *accessObjectController) GenerateAccess(scope *scopes.Scope, identities []string) access_object.ConnectionInformation {
	minioClient := GetMinioClient()

	// Create a bucket
	bucketName := randStringRunes(rand.Intn(5) + 8)
	err := minioClient.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{})
	if err != nil {
		panic(err)
	}

	// TODO : create access based on scope and identities
	// For example create user and attach roles and return their to your new struct

	// get remote URL or stick with local url
	remurl := config.Config.Service.MinioHost
	if config.Config.Service.MinioRemoteUrl == "" {
		//Get the good url from identity
		//TODO Make better way to get remote url
		client := http.Client{}
		if resp, err := client.Get("http://traefik.default.svc.cluster.local/api/discovery/identity"); err != nil {
			log.Println("Error geting remote url", err)
		} else {
			rBytes, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Println("Error reading response remote url", err)
			}
			var response map[string]json.RawMessage
			json.Unmarshal(rBytes, &response)
			if identityJson, ok := response["identity"]; !ok {
				log.Println("Error getting identity", err)
			} else {
				var identity map[string]string
				json.Unmarshal(identityJson, &identity)
				if identityUrl, ok := identity["url"]; !ok {
					log.Println("couldn't get url from body", string(identityJson))
				} else {
					u, err := url.Parse(identityUrl)
					if err != nil {
						log.Println("couldn't get url from body", string(identityJson))
					}
					remurl = fmt.Sprintf("%s:9001", u.Host)
				}

			}

		}
	} else {
		remurl = config.Config.Service.MinioRemoteUrl
	}
	access := &access{
		AccessHost:   remurl,
		AccessKey:    config.Config.Service.MinioAccessKey,
		AccessSecret: config.Config.Service.MinioSecretKey,
		Bucket:       bucketName,
	}

	return access
}

// TODO : Implement PurgeAccess function that purge access that is represented by ConnectionInformation
// You must not touch to function declaration
func (a *accessObjectController) PurgeAccess(connInfo access_object.ConnectionInformation) {
	// You need to unmarshal connInfo to read it
	var access access
	if err := json.Unmarshal(connInfo.([]byte), &access); err != nil {
		panic(err)
	}

	//client := GetProviderClient()

	// TODO : delete access based on connInfo
	// For example delete user and attached roles
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
